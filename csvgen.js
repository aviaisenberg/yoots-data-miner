const { MongoClient } = require("mongodb");
const fs = require("fs");
const fastcsv = require("fast-csv");
require("dotenv").config();

// MongoDB connection URI and database/collection details
const uri = process.env.MONGO_URI;
const dbName = process.env.MONGO_DB_NAME;
const collectionName = process.env.MONGO_COLLECTION_NAME;

// CSV file path
const csvFilePath = "wallets_and_tokens.csv";

async function fetchTokenData() {
	const client = new MongoClient(uri, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	});

	try {
		await client.connect();
		console.log("Connected to MongoDB");

		const db = client.db(dbName);
		const collection = db.collection(collectionName);

		// Perform the aggregation query
		const aggregationResult = await collection
			.aggregate([
				{
					$match: {
						ownerAtBlockHeight: { $ne: null },
					},
				},
				{
					$group: {
						_id: "$ownerAtBlockHeight",
						tokenCount: { $sum: 1 },
					},
				},
				{
					$project: {
						_id: 0,
						walletAddress: "$_id",
						tokenCount: 1,
					},
				},
			])
			.toArray();

		return aggregationResult;
	} catch (e) {
		console.error("An error occurred:", e);
	} finally {
		await client.close();
	}
}

async function writeToCsv(data) {
	const ws = fs.createWriteStream(csvFilePath);
	fastcsv.write(data, { headers: true }).pipe(ws);

	console.log(`Data written to file: ${csvFilePath}`);
}

async function main() {
	const data = await fetchTokenData();
	if (data && data.length) {
		await writeToCsv(data);
	} else {
		console.log("No data to write to CSV");
	}
}

main().catch(console.error);
