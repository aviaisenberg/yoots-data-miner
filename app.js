const { MongoClient } = require("mongodb");
const { ethers } = require("ethers");
require("dotenv").config();

// MongoDB connection URI and database/collection details
const uri = process.env.MONGO_URI;
const dbName = process.env.MONGO_DB_NAME;
const collectionName = process.env.MONGO_COLLECTION_NAME;
// Contract ABI
const abi = [
	{
		inputs: [
			{
				internalType: "contract IWormhole",
				name: "wormhole",
				type: "address",
			},
			{
				internalType: "bytes32",
				name: "emitterAddress",
				type: "bytes32",
			},
			{ internalType: "bytes", name: "baseUri", type: "bytes" },
		],
		stateMutability: "nonpayable",
		type: "constructor",
	},
	{ inputs: [], name: "BaseUriEmpty", type: "error" },
	{ inputs: [], name: "BaseUriTooLong", type: "error" },
	{ inputs: [], name: "FailedToSend", type: "error" },
	{
		inputs: [{ internalType: "string", name: "reason", type: "string" }],
		name: "FailedVaaParseAndVerification",
		type: "error",
	},
	{ inputs: [], name: "InvalidMessageLength", type: "error" },
	{ inputs: [], name: "InvalidMsgValue", type: "error" },
	{ inputs: [], name: "VaaAlreadyClaimed", type: "error" },
	{ inputs: [], name: "WrongEmitterAddress", type: "error" },
	{ inputs: [], name: "WrongEmitterChainId", type: "error" },
	{
		anonymous: false,
		inputs: [
			{
				indexed: false,
				internalType: "address",
				name: "previousAdmin",
				type: "address",
			},
			{
				indexed: false,
				internalType: "address",
				name: "newAdmin",
				type: "address",
			},
		],
		name: "AdminChanged",
		type: "event",
	},
	{
		anonymous: false,
		inputs: [
			{
				indexed: true,
				internalType: "address",
				name: "owner",
				type: "address",
			},
			{
				indexed: true,
				internalType: "address",
				name: "approved",
				type: "address",
			},
			{
				indexed: true,
				internalType: "uint256",
				name: "tokenId",
				type: "uint256",
			},
		],
		name: "Approval",
		type: "event",
	},
	{
		anonymous: false,
		inputs: [
			{
				indexed: true,
				internalType: "address",
				name: "owner",
				type: "address",
			},
			{
				indexed: true,
				internalType: "address",
				name: "operator",
				type: "address",
			},
			{
				indexed: false,
				internalType: "bool",
				name: "approved",
				type: "bool",
			},
		],
		name: "ApprovalForAll",
		type: "event",
	},
	{
		anonymous: false,
		inputs: [
			{
				indexed: false,
				internalType: "uint256[]",
				name: "tokenIds",
				type: "uint256[]",
			},
			{
				indexed: true,
				internalType: "address",
				name: "receiver",
				type: "address",
			},
		],
		name: "BatchMinted",
		type: "event",
	},
	{
		anonymous: false,
		inputs: [
			{
				indexed: true,
				internalType: "address",
				name: "beacon",
				type: "address",
			},
		],
		name: "BeaconUpgraded",
		type: "event",
	},
	{
		anonymous: false,
		inputs: [
			{
				indexed: false,
				internalType: "uint8",
				name: "version",
				type: "uint8",
			},
		],
		name: "Initialized",
		type: "event",
	},
	{
		anonymous: false,
		inputs: [
			{
				indexed: true,
				internalType: "uint256",
				name: "tokenId",
				type: "uint256",
			},
			{
				indexed: true,
				internalType: "address",
				name: "receiver",
				type: "address",
			},
		],
		name: "Minted",
		type: "event",
	},
	{
		anonymous: false,
		inputs: [
			{
				indexed: true,
				internalType: "address",
				name: "previousOwner",
				type: "address",
			},
			{
				indexed: true,
				internalType: "address",
				name: "newOwner",
				type: "address",
			},
		],
		name: "OwnershipTransferStarted",
		type: "event",
	},
	{
		anonymous: false,
		inputs: [
			{
				indexed: true,
				internalType: "address",
				name: "previousOwner",
				type: "address",
			},
			{
				indexed: true,
				internalType: "address",
				name: "newOwner",
				type: "address",
			},
		],
		name: "OwnershipTransferred",
		type: "event",
	},
	{
		anonymous: false,
		inputs: [
			{
				indexed: true,
				internalType: "address",
				name: "from",
				type: "address",
			},
			{
				indexed: true,
				internalType: "address",
				name: "to",
				type: "address",
			},
			{
				indexed: true,
				internalType: "uint256",
				name: "tokenId",
				type: "uint256",
			},
		],
		name: "Transfer",
		type: "event",
	},
	{
		anonymous: false,
		inputs: [
			{
				indexed: true,
				internalType: "address",
				name: "implementation",
				type: "address",
			},
		],
		name: "Upgraded",
		type: "event",
	},
	{
		inputs: [],
		name: "acceptOwnership",
		outputs: [],
		stateMutability: "nonpayable",
		type: "function",
	},
	{
		inputs: [
			{ internalType: "address", name: "to", type: "address" },
			{ internalType: "uint256", name: "tokenId", type: "uint256" },
		],
		name: "approve",
		outputs: [],
		stateMutability: "nonpayable",
		type: "function",
	},
	{
		inputs: [{ internalType: "address", name: "owner", type: "address" }],
		name: "balanceOf",
		outputs: [{ internalType: "uint256", name: "", type: "uint256" }],
		stateMutability: "view",
		type: "function",
	},
	{
		inputs: [],
		name: "deleteDefaultRoyalty",
		outputs: [],
		stateMutability: "nonpayable",
		type: "function",
	},
	{
		inputs: [{ internalType: "uint256", name: "tokenId", type: "uint256" }],
		name: "getApproved",
		outputs: [{ internalType: "address", name: "", type: "address" }],
		stateMutability: "view",
		type: "function",
	},
	{
		inputs: [
			{ internalType: "string", name: "name", type: "string" },
			{ internalType: "string", name: "symbol", type: "string" },
			{
				internalType: "address",
				name: "royaltyReceiver",
				type: "address",
			},
			{
				internalType: "uint96",
				name: "royaltyFeeNumerator",
				type: "uint96",
			},
		],
		name: "initialize",
		outputs: [],
		stateMutability: "nonpayable",
		type: "function",
	},
	{
		inputs: [
			{ internalType: "address", name: "owner", type: "address" },
			{ internalType: "address", name: "operator", type: "address" },
		],
		name: "isApprovedForAll",
		outputs: [{ internalType: "bool", name: "", type: "bool" }],
		stateMutability: "view",
		type: "function",
	},
	{
		inputs: [],
		name: "name",
		outputs: [{ internalType: "string", name: "", type: "string" }],
		stateMutability: "view",
		type: "function",
	},
	{
		inputs: [],
		name: "owner",
		outputs: [{ internalType: "address", name: "", type: "address" }],
		stateMutability: "view",
		type: "function",
	},
	{
		inputs: [{ internalType: "uint256", name: "tokenId", type: "uint256" }],
		name: "ownerOf",
		outputs: [{ internalType: "address", name: "", type: "address" }],
		stateMutability: "view",
		type: "function",
	},
	{
		inputs: [],
		name: "pendingOwner",
		outputs: [{ internalType: "address", name: "", type: "address" }],
		stateMutability: "view",
		type: "function",
	},
	{
		inputs: [],
		name: "proxiableUUID",
		outputs: [{ internalType: "bytes32", name: "", type: "bytes32" }],
		stateMutability: "view",
		type: "function",
	},
	{
		inputs: [{ internalType: "bytes", name: "vaa", type: "bytes" }],
		name: "receiveAndMint",
		outputs: [],
		stateMutability: "nonpayable",
		type: "function",
	},
	{
		inputs: [{ internalType: "bytes", name: "vaa", type: "bytes" }],
		name: "receiveAndMintBatch",
		outputs: [],
		stateMutability: "nonpayable",
		type: "function",
	},
	{
		inputs: [],
		name: "renounceOwnership",
		outputs: [],
		stateMutability: "nonpayable",
		type: "function",
	},
	{
		inputs: [{ internalType: "uint256", name: "tokenId", type: "uint256" }],
		name: "resetTokenRoyalty",
		outputs: [],
		stateMutability: "nonpayable",
		type: "function",
	},
	{
		inputs: [
			{ internalType: "uint256", name: "_tokenId", type: "uint256" },
			{ internalType: "uint256", name: "_salePrice", type: "uint256" },
		],
		name: "royaltyInfo",
		outputs: [
			{ internalType: "address", name: "", type: "address" },
			{ internalType: "uint256", name: "", type: "uint256" },
		],
		stateMutability: "view",
		type: "function",
	},
	{
		inputs: [
			{ internalType: "address", name: "from", type: "address" },
			{ internalType: "address", name: "to", type: "address" },
			{ internalType: "uint256", name: "tokenId", type: "uint256" },
		],
		name: "safeTransferFrom",
		outputs: [],
		stateMutability: "nonpayable",
		type: "function",
	},
	{
		inputs: [
			{ internalType: "address", name: "from", type: "address" },
			{ internalType: "address", name: "to", type: "address" },
			{ internalType: "uint256", name: "tokenId", type: "uint256" },
			{ internalType: "bytes", name: "data", type: "bytes" },
		],
		name: "safeTransferFrom",
		outputs: [],
		stateMutability: "nonpayable",
		type: "function",
	},
	{
		inputs: [
			{ internalType: "address", name: "operator", type: "address" },
			{ internalType: "bool", name: "approved", type: "bool" },
		],
		name: "setApprovalForAll",
		outputs: [],
		stateMutability: "nonpayable",
		type: "function",
	},
	{
		inputs: [
			{ internalType: "address", name: "receiver", type: "address" },
			{ internalType: "uint96", name: "feeNumerator", type: "uint96" },
		],
		name: "setDefaultRoyalty",
		outputs: [],
		stateMutability: "nonpayable",
		type: "function",
	},
	{
		inputs: [
			{ internalType: "uint256", name: "tokenId", type: "uint256" },
			{ internalType: "address", name: "receiver", type: "address" },
			{ internalType: "uint96", name: "feeNumerator", type: "uint96" },
		],
		name: "setTokenRoyalty",
		outputs: [],
		stateMutability: "nonpayable",
		type: "function",
	},
	{
		inputs: [
			{ internalType: "bytes4", name: "interfaceId", type: "bytes4" },
		],
		name: "supportsInterface",
		outputs: [{ internalType: "bool", name: "", type: "bool" }],
		stateMutability: "view",
		type: "function",
	},
	{
		inputs: [],
		name: "symbol",
		outputs: [{ internalType: "string", name: "", type: "string" }],
		stateMutability: "view",
		type: "function",
	},
	{
		inputs: [{ internalType: "uint256", name: "tokenId", type: "uint256" }],
		name: "tokenURI",
		outputs: [{ internalType: "string", name: "", type: "string" }],
		stateMutability: "view",
		type: "function",
	},
	{
		inputs: [
			{ internalType: "address", name: "from", type: "address" },
			{ internalType: "address", name: "to", type: "address" },
			{ internalType: "uint256", name: "tokenId", type: "uint256" },
		],
		name: "transferFrom",
		outputs: [],
		stateMutability: "nonpayable",
		type: "function",
	},
	{
		inputs: [
			{ internalType: "address", name: "newOwner", type: "address" },
		],
		name: "transferOwnership",
		outputs: [],
		stateMutability: "nonpayable",
		type: "function",
	},
	{
		inputs: [
			{
				internalType: "address",
				name: "newImplementation",
				type: "address",
			},
		],
		name: "upgradeTo",
		outputs: [],
		stateMutability: "nonpayable",
		type: "function",
	},
	{
		inputs: [
			{
				internalType: "address",
				name: "newImplementation",
				type: "address",
			},
			{ internalType: "bytes", name: "data", type: "bytes" },
		],
		name: "upgradeToAndCall",
		outputs: [],
		stateMutability: "payable",
		type: "function",
	},
];

// Ethereum provider URL and contract details
const ethereumProviderUrl = process.env.ETHEREUM_PROVIDER_URL;
const ethereumContractAddress = "0xFd1b0b0dfa524E1fD42E7d51155A663c581bBd50";

// Polygon provider URL and contract details
const polygonProviderUrl = process.env.POLYGON_PROVIDER_URL;
const polygonContractAddress = "0x670fd103b1a08628e9557cD66B87DeD841115190";

// Specific Ethereum block height
const blockHeight = 18911559;

async function main() {
	const client = new MongoClient(uri);

	try {
		await client.connect();
		console.log("Connected to MongoDB");

		const db = client.db(dbName);
		const collection = db.collection(collectionName);

		const aggregationResult = await collection
			.aggregate([
				{
					$match: {
						attributes: {
							$elemMatch: {
								trait_type: "Eyewear",
								value: "Nouns",
							},
						},
					},
				},
				{
					$group: {
						_id: null,
						tokenIds: { $push: "$tokenId" },
					},
				},
			])
			.toArray();

		const tokenIds = aggregationResult[0]?.tokenIds || [];
		console.log({ tokenIds });

		const ethereumProvider = new ethers.JsonRpcProvider(
			ethereumProviderUrl
		);
		const ethereumContract = new ethers.Contract(
			ethereumContractAddress,
			abi,
			ethereumProvider
		);

		const polygonProvider = new ethers.JsonRpcProvider(polygonProviderUrl);
		const polygonContract = new ethers.Contract(
			polygonContractAddress,
			abi,
			polygonProvider
		);

		for (const tokenId of tokenIds) {
			try {
				const owner = await ethereumContract.ownerOf(tokenId, {
					blockTag: blockHeight,
				});
				console.log(
					`Token ID ${tokenId} found on Ethereum, owned by ${owner}`
				);
				await collection.updateOne(
					{ tokenId: tokenId },
					{
						$set: {
							ownerAtBlockHeight: owner,
							blockchain: "Ethereum",
						},
					}
				);
			} catch (ethereumError) {
				try {
					const owner = await polygonContract.ownerOf(tokenId);
					console.log(
						`Token ID ${tokenId} found on Polygon, owned by ${owner}`
					);
					await collection.updateOne(
						{ tokenId: tokenId },
						{
							$set: {
								ownerAtBlockHeight: owner,
								blockchain: "Polygon",
							},
						}
					);
				} catch (polygonError) {
					console.error(
						`Token ID ${tokenId} not found on Ethereum or Polygon`
					);
				}
			}
		}

		console.log("Update complete");
	} catch (e) {
		console.error(e);
	} finally {
		await client.close();
	}
}

main().catch(console.error);
